;;; whitey-theme.el --- whitey for Emacs.

;; Copyright (C) 2015 Alexander Adhyatma

;; Author: Alexander Adhyatma
;; URL: http:/gitlab.com/franksn/whitey-theme/
;; Version: 0.3.0
;;
;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A colorscheme for emacs with light and dark variant of it.
;;
;;; Installation:
;;
;;   M-x package-install -> whitey-theme
;;
;;   (load-theme 'whitey-dark t)
;;     or
;;   (load-theme 'whitey-light t)
;;
;;; Bugs
;;
;; report any bugs to franksn@openmailbox.org
;;
;;; Code:

(require 'whitey-common)

(deftheme whitey-dark "whitey theme, the dark version")

(create-whitey-theme 'dark 'whitey-dark)

(provide-theme 'whitey-dark)
